# Generated by Django 2.0.6 on 2018-06-13 21:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ganesa', '0004_device_is_online'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='is_online',
            field=models.BooleanField(default=False),
        ),
    ]
