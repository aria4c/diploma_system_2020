$(document).ready(function(){
	var deviceId = $('#deviceId').val();

    var socket = new WebSocket(
        'ws://' + window.location.host +
        '/data/' + deviceId+'/');
	socket.onmessage = websocket_msj_received;
	$('#formulario').submit(function(e){
	});
});

function websocket_msj_received(e){
     inputData = JSON.parse(e.data);
     message = inputData.message;
     data = message.message.data;
     sensorName=message.sensor_name;
	//data = message.data;
//	sensorName = data.sensor_name;

	//updateChart(sensorName,data);


/*	$('#dataX').html(data.message.message.X);
	$('#dataY').html(data.message.message.Y);
	$('#dataZ').html(data.message.message.Z);*/
	//updateChart(data)


	var index = $('#sensor-chart-'+get_chart_name(sensorName)).data('highchartsChart');
	var chart = Highcharts.charts[index];

	updateChart(data, chart);


}


function updateChart(data, chart) {

	var shift = chart.series[0].data.length > 20;

	if(chart.series[0].length > 150)
	{
		chart.series[0].remove()
	}
	if(chart.series[1].length > 150)
	{
		chart.series[1].remove()
	}
	if(chart.series[1].length > 150)
	{
		chart.series[1].remove()
	}
            chart.series[0].addPoint(data.X, true, shift);
            chart.series[1].addPoint(data.Y, true, shift);
            chart.series[2].addPoint(data.Z, true, shift);


}

function get_chart_name(sensorName) {
	return sensorName.toLowerCase().replace(' ','-');
}