import json

from asgiref.sync import async_to_sync
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.generic.websocket import WebsocketConsumer

from ganesa.models import Device, DeviceLogListeners
from ganesa.models import Sensor
from ganesa.models import SensorData
from ganesa.utils.hashmap import HashMap

authenticated_devices = HashMap()
streaming_devices = HashMap()

listener_group = "listener_group"


class ListenerConsumer(WebsocketConsumer):
    def connect(self):
        print("Connected new listener")

        async_to_sync(self.channel_layer.group_add)(
            listener_group,
            self.channel_name
        )

        self.accept()

    def disconnect(self, code):
        async_to_sync(self.channel_layer.group_discard)(
            listener_group,
            self.channel_name
        )

    def listener_message(self, event):
        message = event['message']
        self.send(text_data=json.dumps({
            'message': message
        }))


class AuthConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        print("Connecting new device")
        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def receive(self, text_data=None, bytes_data=None):
        try:
            data = json.loads(text_data)
        except json.JSONDecodeError:
            await self.close()
            return

        device_id = data['device_id']
        if device_id is None:
            await self.close()
            return

        try:
            device = Device.objects.get(pk=device_id)
        except Device.DoesNotExist:
            device = Device(device_id=data['device_id'], name=data['name'])
            device.save()
            await self.channel_layer.group_send(
                listener_group,
                {
                    'type': 'listener_message',
                    'message': {
                        'message_type': 'device_registered',
                        'data': {
                            'device_id': device_id,
                            'device_name': device.name
                        }
                    }
                }
            )

        for item in data['sensors']:
            try:
                sensor = Sensor.objects.get(name=item['name'])
            except Sensor.DoesNotExist:
                sensor = Sensor()
                sensor.name = item['name']
                sensor.type = item['type']
                database_sync_to_async(sensor.save())()
                sensor = Sensor.objects.get(name=sensor.name)

            device.sensors.add(sensor)

        device.is_authenticated = True
        database_sync_to_async(device.save())()

        if authenticated_devices.get(device_id) is None:
            authenticated_devices.add(device_id, device)

        await self.send(text_data=json.dumps({
            'auth': True
        }))
        await self.close()


class StreamingDevice(WebsocketConsumer):
    device_id = None
    group_name = None

    def connect(self):
        self.device_id = self.scope['url_route']['kwargs']['device_id']
        self.group_name = 'device_%s' % self.device_id

        print("Connecting to: ", self.device_id, self.group_name)

        device = authenticated_devices.get(self.device_id)

        if device is None:
            self.close()
            return

        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )

        self.accept()

        streaming_devices.add(self.channel_name, device.device_id)

    #    device.is_online = True
        database_sync_to_async(device.save())()

        authenticated_devices.add(self.device_id, device)

        async_to_sync(self.channel_layer.group_send)(
            listener_group,
            {
                'type': 'listener_message',
                'message': {
                    'message_type': 'device_streaming_start',
                    'data': {
                        'device_id': self.device_id
                    }
                }
            }
        )

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )
        device_id = streaming_devices.get(self.channel_name)

        if device_id is None:
            return

        device = authenticated_devices.get(device_id)

        if device is None:
            return

        device.is_online = False
        database_sync_to_async(device.save())

        authenticated_devices.add(self.device_id, device)

        async_to_sync(self.channel_layer.group_send)(
            listener_group,
            {
                'type': 'listener_message',
                'message': {
                    'message_type': 'device_disconnected',
                    'data': {
                        'device_id': device.device_id
                    }
                }
            }
        )

    def receive(self, text_data=None, bytes_data=None):
        try:
            data = json.loads(text_data)
        except json.JSONDecodeError:
            self.close()
            return



        device = Device.objects.get(device_id=self.device_id)

        if data is not None:
            if device.is_online is not True:
                device.is_online = True
                database_sync_to_async(device.save())


        async_to_sync(self.channel_layer.group_send)(
            self.group_name,
            {
                'type': 'message',
                'message': data
            }
        )

        # sensor = Sensor(name=data['data_type'])
        # print(data)

        sensor_data = SensorData()
        sensor_data.device_id = device
        sensor_data.sensor = Sensor.objects.get(name=data['sensor_name'])
        sensor_data.meta_data = data['message']['data']
        sensor_data.data = data['message']['meta_data']
        listener = DeviceLogListeners.objects.filter(device=device, sensor=sensor_data.sensor).first()
        if listener:
            result = listener.check_limits(sensor_data.meta_data)
            if result:
                async_to_sync(self.channel_layer.group_send)(
                    listener_group,
                    {
                        'type': 'listener_message',
                        'message': {
                            'message_type': 'device_limit',
                            'data': result
                        }
                    }
                )
        database_sync_to_async(sensor_data.save())()

    def message(self, event):
        message = event['message']
        self.send(text_data=json.dumps({
            'message': message
        }))
