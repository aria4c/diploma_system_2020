from channels.routing import ProtocolTypeRouter, URLRouter
import ganesa.routing

application = ProtocolTypeRouter({
    'websocket': URLRouter(
        ganesa.routing.websocket_urlpatterns
    )
})
