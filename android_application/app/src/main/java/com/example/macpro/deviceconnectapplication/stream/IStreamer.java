package com.example.macpro.deviceconnectapplication.stream;

public interface IStreamer {
    void run();
    void stop();
}
