package com.example.macpro.deviceconnectapplication.stream;

import android.os.Build;

import com.example.macpro.deviceconnectapplication.SendDataActivity;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StreamClient {
    private WebSocket socket;

    private URI uri;

    private List<IStreamer> streamers = new ArrayList<>();

    private boolean isStreaming = false;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public StreamClient(SendDataActivity sendDataActivity, String serverIp, String serverPort) {
        try {
            this.uri = new URI("ws://" + serverIp + ":" + serverPort + "/data/" + UUID.nameUUIDFromBytes(Build.SERIAL.getBytes()).toString());
            this.socket = new WebSocketFactory().createSocket(
                    this.uri);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        streamers.add(new AccelerometerStreamer(sendDataActivity));
        streamers.add(new MagneticFieldStreamer(sendDataActivity));
        streamers.add(new TemperatureStreamer(sendDataActivity));
    }


    public void connect() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new WebSocketFactory().createSocket(uri);
                    socket.connect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public synchronized void sendData(final String data) {
        if(this.socket.isOpen()) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    socket.sendText(data);
                }
            });
        }
    }

    public boolean isConnected() {
        return this.socket.isOpen();
    }

    public boolean isStreaming() {
        return isStreaming;
    }

    public void startStreaming() {
        if(!isConnected()) this.connect();
        for(IStreamer streamer : streamers) {
            streamer.run();
        }
        this.isStreaming = true;
    }

    public void stopStreaming() {
        if(isConnected()) this.socket.disconnect();
        for(IStreamer streamer : streamers) {
            streamer.stop();
        }
        this.isStreaming = false;
    }


}
