package com.example.macpro.deviceconnectapplication.stream;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.macpro.deviceconnectapplication.SendDataActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class MagneticFieldStreamer implements IStreamer {
    public float mfSensorX, mfSensorY, mfSensorZ;
    public JSONObject metaData = new JSONObject();
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private ScheduledFuture<?> mfFuture;

    private SendDataActivity sendDataActivity;


    public MagneticFieldStreamer(SendDataActivity sendDataActivity) {
        SensorManager sensorManager = (SensorManager) sendDataActivity.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).get(0);
        sensorManager.registerListener(mfListener, sensor,
                SensorManager.SENSOR_DELAY_FASTEST);

        this.sendDataActivity = sendDataActivity;
    }
    private SensorEventListener mfListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int acc) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            mfSensorX = event.values[0];
            mfSensorY = 1;//event.values[1];
            mfSensorZ = 1;//event.values[2];
            Date currentTime = Calendar.getInstance().getTime();
            try {
                metaData.put("time",System.currentTimeMillis());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    public void run() {
        this.mfFuture = executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject accelerometerData = new JSONObject();

                    accelerometerData.put("X", mfSensorX);
                    accelerometerData.put("Y", mfSensorY);
                    accelerometerData.put("Z", mfSensorZ);

                    JSONObject sensorDataObject = new JSONObject();
                    sensorDataObject.put("data",accelerometerData);
                    sensorDataObject.put("meta_data",metaData);

                    final JSONObject message = new JSONObject();

                    message.put("sensor_name", "Magnetic Field");
                    message.put("message", sensorDataObject);

                    if(sendDataActivity.getStreamClient().isConnected()) {
                        sendDataActivity.getStreamClient().sendData(message.toString());
                    }

                    //sendDataActivity.accDataLabel.setText(message.toString());
                } catch (Exception ex) {
                    Log.e("Error", "Error: " + ex.getMessage());
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stop() {
        this.mfFuture.cancel(true);
    }
}
