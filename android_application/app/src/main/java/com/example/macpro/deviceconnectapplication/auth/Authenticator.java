package com.example.macpro.deviceconnectapplication.auth;

import android.os.Build;
import android.util.Log;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Authenticator extends WebSocketAdapter {
    private final String serverIp;
    private final String serverPort;

    private URI uri;

    private WebSocket socket;

    private JSONObject authData;

    private volatile boolean isAuthenticated = false;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private static Authenticator instance;

    public Authenticator(String serverIp, String serverPort) {
        instance = this;
        this.serverIp = serverIp;
        this.serverPort = serverPort;
        try {
            this.uri = new URI("ws://" + this.serverIp + ":" + this.serverPort + "/auth/");
        } catch (URISyntaxException ex) {
            Log.e("Error", "Error while creating URI. URI is incorrect!");
        }
        try {
            this.socket = new WebSocketFactory().createSocket(this.uri);
        } catch (IOException e) {
            Log.e("Error", "Error");
        }
        this.socket.addListener(this);
    }

    public void setAuthData(JSONArray sensors) {
        JSONObject authData = new JSONObject();
        try {
            authData.put("device_id", UUID.nameUUIDFromBytes(Build.SERIAL.getBytes()).toString());
            authData.put("name", "Android#" + Build.SERIAL);
            authData.put("sensors", sensors);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.authData = authData;
    }

    public void auth() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new WebSocketFactory().createSocket(uri);
                    socket.addListener(instance);
                    socket.connect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (socket.isOpen()) {
                    socket.sendText(authData.toString());
                }
            }
        });
    }

    @Override
    public void onTextMessage(WebSocket websocket, String message) throws Exception {
        JSONObject object = new JSONObject(message);
        if(object.getBoolean("auth")) {
            this.isAuthenticated = true;
        }
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }
}
