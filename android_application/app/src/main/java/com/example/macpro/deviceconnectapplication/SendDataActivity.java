package com.example.macpro.deviceconnectapplication;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.macpro.deviceconnectapplication.auth.Authenticator;
import com.example.macpro.deviceconnectapplication.stream.StreamClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SendDataActivity extends AppCompatActivity {
    private Switch accSwitch;
    private Switch mfSwitch;

    public TextView authorizeDebugLabel;
    public TextView accDataLabel;
    private TextView accDebugLabel;
    private TextView debugLabel;

    private Button authButton;

    private String serverIp;
    private String serverPort;

    private Authenticator authenticator;
    private StreamClient streamClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_data);

        this.debugLabel = findViewById(R.id.debug_label);
        this.accDebugLabel = findViewById(R.id.acc_debug_label);
        this.accDebugLabel.setTextColor(Color.RED);
        this.accDebugLabel.setText("Offline");
        this.accSwitch=findViewById(R.id.acc_switch);
        this.authButton = findViewById(R.id.atuhorize_button);

        Intent intent = getIntent();

        this.serverIp = intent.getStringExtra("serverIp");
        this.serverPort = intent.getStringExtra("serverPort");
        this.debugLabel.setText("Loaded settings:\n Server IP: "+this.serverIp+" Server Port: "+this.serverPort);

        this.authenticator = new Authenticator(this.serverIp, this.serverPort);
        this.streamClient = new StreamClient(this, this.serverIp, this.serverPort);

        View.OnClickListener oclAuth = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authenticator.setAuthData(collectSensors());
                authenticator.auth();
            }
        };

        View.OnClickListener oclDisc = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };

        CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(authenticator.isAuthenticated()) {
                    if (!streamClient.isStreaming()) {
                        accDebugLabel.setTextColor(Color.GREEN);
                        accDebugLabel.setText("Online");
                        streamClient.startStreaming();
                    } else {
                        streamClient.stopStreaming();
                        accDebugLabel.setTextColor(Color.RED);
                        accDebugLabel.setText("Offline");
                    }
                } else {
                    accDebugLabel.setTextColor(Color.RED);
                    accDebugLabel.setText("Not auth");
                }
            }
        };

        this.accSwitch.setOnCheckedChangeListener(onCheckedChangeListener);
        this.authButton.setOnClickListener(oclAuth);

        Button disconnectButton = findViewById(R.id.disconnect_button);
        disconnectButton.setOnClickListener(oclDisc);
    }


    private JSONArray collectSensors() {
        JSONArray sensors = new JSONArray();

        JSONObject accelerometerSensor = new JSONObject();
        JSONObject gyroscopeSensor = new JSONObject();
        JSONObject magneticFieldSensor = new JSONObject();
        JSONObject temperatureSensor = new JSONObject();


        try {
            accelerometerSensor.put("name", "Accelerometer");
            accelerometerSensor.put("type", "XYZ");
            gyroscopeSensor.put("name", "Gyroscope");
            gyroscopeSensor.put("type", "XYZ");;
            magneticFieldSensor.put("name", "Magnetic Field");
            magneticFieldSensor.put("type", "XYZ");;
            temperatureSensor.put("name", "Temperature");
            temperatureSensor.put("type", "TEMP");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sensors.put(accelerometerSensor);
        sensors.put(gyroscopeSensor);
        sensors.put(magneticFieldSensor);
        sensors.put(temperatureSensor);
        return sensors;
    }

    public StreamClient getStreamClient() {
        return streamClient;
    }

    public Authenticator getAuthenticator() {
        return authenticator;
    }
}
