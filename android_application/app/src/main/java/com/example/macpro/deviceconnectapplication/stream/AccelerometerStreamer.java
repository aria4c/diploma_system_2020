package com.example.macpro.deviceconnectapplication.stream;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.macpro.deviceconnectapplication.SendDataActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class AccelerometerStreamer implements IStreamer {
    public float accelerometerSensorX, accelerometerSensorY, accelerometerSensorZ;
    public JSONObject metaData = new JSONObject();
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private ScheduledFuture<?> accelerometerFuture;

    private SendDataActivity sendDataActivity;

    public AccelerometerStreamer(SendDataActivity sendDataActivity) {
        SensorManager sensorManager = (SensorManager) sendDataActivity.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
        sensorManager.registerListener(accelerationListener, sensor,
                SensorManager.SENSOR_DELAY_FASTEST);

        this.sendDataActivity = sendDataActivity;
    }

    private SensorEventListener accelerationListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int acc) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            accelerometerSensorX = event.values[0];
            accelerometerSensorY = event.values[1];
            accelerometerSensorZ = event.values[2];
            Date currentTime = Calendar.getInstance().getTime();
            try {
                metaData.put("time",System.currentTimeMillis());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void run() {
        this.accelerometerFuture = executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject accelerometerData = new JSONObject();

                    accelerometerData.put("X", accelerometerSensorX);
                    accelerometerData.put("Y", accelerometerSensorY);
                    accelerometerData.put("Z", accelerometerSensorZ);

                    JSONObject sensorDataObject = new JSONObject();
                    sensorDataObject.put("data",accelerometerData);
                    sensorDataObject.put("meta_data",metaData);

                    final JSONObject message = new JSONObject();

                    message.put("sensor_name", "Accelerometer");
                    message.put("message", sensorDataObject);

                    if(sendDataActivity.getStreamClient().isConnected()) {
                        sendDataActivity.getStreamClient().sendData(message.toString());
                    }

                    //sendDataActivity.accDataLabel.setText(message.toString());
                } catch (Exception ex) {
                    Log.e("Error", "Error: " + ex.getMessage());
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stop() {
        this.accelerometerFuture.cancel(true);
    }
}
