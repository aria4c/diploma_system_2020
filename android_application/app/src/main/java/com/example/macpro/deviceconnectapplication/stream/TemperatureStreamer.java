package com.example.macpro.deviceconnectapplication.stream;

import android.content.Context;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.macpro.deviceconnectapplication.SendDataActivity;

import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TemperatureStreamer implements IStreamer{

    public JSONObject metaData = new JSONObject();
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private ScheduledFuture<?> mfFuture;

    private SendDataActivity sendDataActivity;

    public TemperatureStreamer(SendDataActivity sendDataActivity) {
        SensorManager sensorManager = (SensorManager) sendDataActivity.getSystemService(Context.SENSOR_SERVICE);

        this.sendDataActivity = sendDataActivity;
    }

    @Override
    public void run() {
        this.mfFuture = executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject accelerometerData = new JSONObject();

                    accelerometerData.put("X", 1);
                    accelerometerData.put("Y", 2);
                    accelerometerData.put("Z", 3);

                    JSONObject sensorDataObject = new JSONObject();
                    sensorDataObject.put("data",accelerometerData);
                    sensorDataObject.put("meta_data",metaData);

                    final JSONObject message = new JSONObject();

                    message.put("sensor_name", "Temperature");
                    message.put("message", sensorDataObject);

                    if(sendDataActivity.getStreamClient().isConnected()) {
                        sendDataActivity.getStreamClient().sendData(message.toString());
                    }

                    //sendDataActivity.accDataLabel.setText(message.toString());
                } catch (Exception ex) {
                    Log.e("Error", "Error: " + ex.getMessage());
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stop() {

    }
}
