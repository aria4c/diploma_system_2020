package com.example.macpro.deviceconnectapplication;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.text.TextUtils;

import com.example.macpro.deviceconnectapplication.utils.IpUtils;

public class MainActivity extends AppCompatActivity {

    private EditText ipTextView;

    private EditText portTextView;

    private TextView debugLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.ipTextView = findViewById(R.id.server_ip);

        this.portTextView = findViewById(R.id.server_port);

        Button connectButton = findViewById(R.id.connect_button);

        this.debugLabel = findViewById(R.id.debug_label);

        View.OnClickListener oclConnect = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serverIp = ipTextView.getText().toString();
                String serverPort = portTextView.getText().toString();
                if (TextUtils.isDigitsOnly(serverPort) && IpUtils.validIP(serverIp)) {
                    debugLabel.setTextColor(Color.GREEN);
                    debugLabel.setText("OK");

                    Intent intent = new Intent(MainActivity.this, SendDataActivity.class);
                    intent.putExtra("serverIp", serverIp);
                    intent.putExtra("serverPort", serverPort);
                    MainActivity.this.startActivity(intent);
                } else {
                    debugLabel.setTextColor(Color.RED);
                    debugLabel.setText("NOT OK: ");
                    if (!TextUtils.isDigitsOnly(serverPort)) debugLabel.append("PORT IS INVALID. ");
                    if (!IpUtils.validIP(serverIp)) debugLabel.append("IP IS INVALID ");
                }
            }
        };


        connectButton.setOnClickListener(oclConnect);
    }


}
